/**
 * 检查是否为移动端
 * @returns {booean}
 */
declare function isMobile(): boolean;
export default isMobile;
