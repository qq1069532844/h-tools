/**
 * 往目标元素前后插入其子元素后前的克隆
 * @param parent
 * @param htmlElementArr
 * @returns {HTMLElement[]}
 */
declare function insertHeadAndTail(parent: HTMLElement, htmlElementArr: HTMLElement[]): HTMLElement[];
export default insertHeadAndTail;
