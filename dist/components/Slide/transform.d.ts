/**
 * 设置目标元素的 translate3d、transition属性
 * @param el
 * @param translate3d
 * @param transition
 */
declare function transform(el: HTMLElement, translate3d?: [number, number], transition?: number): void;
export default transform;
