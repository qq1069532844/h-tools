/**
 * 获取视口宽度和高度并返回
 * @returns {[w,h]}
 */
declare function getWindowWAndH(): [number, number];
export default getWindowWAndH;
