/**
 * 获取目标数组的最小值的索引
 * @param array
 * @returns {number}
 */
declare function getMinOfArrayLine(array: number[]): number;
export default getMinOfArrayLine;
