

interface INode {
    val: any,
    next: INode | null
}

export {
    INode
}
